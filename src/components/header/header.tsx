import React from 'react';
import { AppBar, Container } from '@material-ui/core';
import './style.scss';
import { Link } from 'gatsby';
import { StaticImage } from 'gatsby-plugin-image';

const Header = () => {
  return (
    <Container maxWidth="lg" style={{ padding: 0 }}>
      <nav className="navbar">
        <div className="navbar__container">
          <div className="navbar__brand">
            <Link to="/" className="navbar-item">
              <StaticImage
                alt="code"
                src="https://img.icons8.com/pastel-glyph/64/000000/code--v2.png"
              />
            </Link>
          </div>
          <div className="navbar__menu">
            <div className="navbar__menu__start" />
            <div className="navbar__menu__end">
              <Link className="navbar-item" to="/projects">
                Projects
              </Link>
              <div className="navbar-item">
                <Link
                  to="/contact-me"
                  className="button is-primary is-outlined is-rounded"
                >
                  Contact
                </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </Container>
  );
};

export default Header;
