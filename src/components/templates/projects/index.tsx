import React, { useEffect, useMemo } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { Button, Chip, ImageList, ImageListItem } from '@material-ui/core';
import Img from 'gatsby-image';
import { Code, Computer } from '@material-ui/icons';

import FullWithLayout from '../../layouts/full-width/';
import HalfWidthLayout from '../../layouts/half-width/';
import { Stacks } from '../../../utils/stakcs';
import { Tags } from '../../../utils/tags';
import '../../../static/style/home-style.scss';
import { StaticImage } from 'gatsby-plugin-image';

export const query = graphql`
  query ($slug: String!) {
    allDataJson(filter: { slug: { eq: $slug } }) {
      edges {
        node {
          description
          developmenStack
          id
          images {
            url {
              publicURL
              childImageSharp {
                fluid(maxWidth: 1920, quality: 100) {
                  src
                  srcSet
                  aspectRatio
                  sizes
                  base64
                }
              }
            }
          }
          repository
          layout
          siteLink
          slug
          tags
          title
        }
      }
    }
  }
`;

type ProjectsDetailType = {
  allDataJson: {
    edges: [
      {
        node: {
          slug: string;
          layout: 'full-width' | 'half-width';
          repository: string;
          siteLink: string;
          tags: number[];
          title: string;
          description: string;
          developmenStack: number[];
          id: number;
          images: any;
        };
      }
    ];
  };
};

const ProjectsDetail = ({ data }: { data: ProjectsDetailType }) => {
  const { allDataJson } = data;

  const DevelopmentStack = useMemo(() => {
    const developmenStack = [];

    allDataJson.edges[0].node.developmenStack.forEach(nodeStack => {
      Stacks.forEach(stack => {
        if (stack.id === nodeStack) {
          developmenStack.push(stack);
        }
      });
    });

    return developmenStack;
  }, [allDataJson]);

  const PostTags = useMemo(() => {
    const tags = [];

    console.log(allDataJson);

    allDataJson.edges[0].node.tags.forEach(nodeTags => {
      Tags.forEach(tag => {
        if (tag.id === nodeTags) {
          tags.push(tag);
        }
      });
    });

    return tags;
  }, [allDataJson]);

  return (
    <>
      {allDataJson.edges[0].node.layout === 'full-width' ? (
        <FullWithLayout>
          <div className="section is-medium">
            <div className="columns">
              <div className="column">
                <h1 className="title is-spaced is-size-1-desktop is-size-2-tablet is-size-3-mobile">
                  {allDataJson.edges[0].node.title}
                </h1>
                <h2 className="subtitle is-size-4-desktop">Description</h2>
                <p>{allDataJson.edges[0].node.description}</p>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{ marginTop: '1.5rem' }}
                >
                  Development stack
                </h2>
                <div>
                  {DevelopmentStack.map(stack => (
                    <Chip
                      color="primary"
                      style={
                        stack.id !== DevelopmentStack[0].id
                          ? {
                              marginLeft: '10px',
                              backgroundColor: '#7510f7',
                            }
                          : { backgroundColor: '#7510f7' }
                      }
                      icon={<Computer />}
                      key={stack.id}
                      onDelete={undefined}
                      label={stack.name}
                    />
                  ))}
                </div>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{ marginTop: '1.5rem' }}
                >
                  Tags
                </h2>
                <div>
                  {PostTags.map(tag => (
                    <Chip
                      color="secondary"
                      style={
                        tag.id !== PostTags[0].id
                          ? { marginLeft: '10px', backgroundColor: '#141c3a' }
                          : { backgroundColor: '#141c3a' }
                      }
                      icon={<Code />}
                      key={tag.id}
                      onDelete={undefined}
                      label={tag.name}
                    />
                  ))}
                </div>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{
                    marginTop: '1.5rem',
                  }}
                >
                  Image gallery
                </h2>
                <ImageList rowHeight={160} cols={3}>
                  {allDataJson.edges[0].node.images.map(item => (
                    <ImageListItem
                      key={item.url}
                      cols={window.screen.width < 720 ? 3 : 1}
                      style={{ height: '220px' }}
                    >
                      <Img
                        fluid={item.url.childImageSharp.fluid}
                        style={{
                          height: '220px',
                          width: '100%',
                        }}
                        alt="image"
                      />
                    </ImageListItem>
                  ))}
                </ImageList>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{
                    marginTop: '1.5rem',
                  }}
                >
                  <a
                    href={allDataJson.edges[0].node.siteLink}
                    style={{
                      color: 'rgba(10, 10, 10, 0.9)',
                      textDecoration: 'underline',
                    }}
                    rel="noopener noreferrer"
                  >
                    You can visit this site.
                  </a>
                </h2>
              </div>
            </div>
          </div>
        </FullWithLayout>
      ) : (
        <HalfWidthLayout>
          <div className="section is-medium">
            <div className="columns">
              <div className="column">
                <h1 className="title is-spaced is-size-1-desktop is-size-2-tablet is-size-3-mobile">
                  {allDataJson.edges[0].node.title}
                </h1>
                <h2 className="subtitle is-size-4-desktop">Description</h2>
                <p>{allDataJson.edges[0].node.description}</p>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{ marginTop: '1.5rem' }}
                >
                  Development stack
                </h2>
                <div>
                  {DevelopmentStack.map(stack => (
                    <Chip
                      color="primary"
                      style={
                        stack.id !== DevelopmentStack[0].id
                          ? {
                              marginLeft: '10px',
                              backgroundColor: '#7510f7',
                            }
                          : { backgroundColor: '#7510f7' }
                      }
                      icon={<Computer />}
                      key={stack.id}
                      onDelete={undefined}
                      label={stack.name}
                    />
                  ))}
                </div>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{ marginTop: '1.5rem' }}
                >
                  Tags
                </h2>
                <div>
                  {PostTags.map(tag => (
                    <Chip
                      color="secondary"
                      style={
                        tag.id !== PostTags[0].id
                          ? { marginLeft: '10px', backgroundColor: '#141c3a' }
                          : { backgroundColor: '#141c3a' }
                      }
                      icon={<Code />}
                      key={tag.id}
                      onDelete={undefined}
                      label={tag.name}
                    />
                  ))}
                </div>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{
                    marginTop: '1.5rem',
                  }}
                >
                  Image gallery
                </h2>
                <ImageList rowHeight={160} cols={3}>
                  {allDataJson.edges[0].node.images.map(item => (
                    <ImageListItem
                      key={item.url}
                      cols={1}
                      style={{ height: '220px' }}
                    >
                      <Img
                        fluid={item.url.childImageSharp.fluid}
                        style={{
                          height: '220px',
                          width: '100%',
                        }}
                        alt="image"
                      />
                    </ImageListItem>
                  ))}
                </ImageList>
                <h2
                  className="subtitle is-size-4-desktop"
                  style={{
                    marginTop: '1.5rem',
                  }}
                >
                  <a
                    href={allDataJson.edges[0].node.siteLink}
                    style={{
                      color: 'rgba(10, 10, 10, 0.9)',
                      textDecoration: 'underline',
                    }}
                    rel="noopener noreferrer"
                  >
                    You can visit this site.
                  </a>
                </h2>
              </div>
            </div>
          </div>
        </HalfWidthLayout>
      )}
    </>
  );
};

export default ProjectsDetail;
