import { Container } from '@material-ui/core';
import React from 'react';
import Header from '../../header';
import Footer from '../../footer';

const FullWithLayout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <>
        {children}
      </>
      <Footer />
    </>
  );
};

export default FullWithLayout;
