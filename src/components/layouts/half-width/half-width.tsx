import { Container } from '@material-ui/core';
import React from 'react';
import Header from '../../header/';
import Footer from '../../footer/';

const HalfWidthLayout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <Container maxWidth="lg">{children}</Container>
      <Footer />
    </>
  );
};

export default HalfWidthLayout;
