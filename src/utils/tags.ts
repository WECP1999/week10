export const Tags = [
  { id: 1, name: 'js', description: 'JavaScript' },
  { id: 2, name: 'ts', description: 'TypeScript' },
  { id: 3, name: 'react', description: 'React' },
  { id: 4, name: 'testing', description: 'Testing' },
  { id: 5, name: 'gatsby', description: 'Gatsby' },
];
