export const Stacks = [
  { id: 1, name: 'WebStorm' },
  { id: 2, name: 'JavaScript' },
  { id: 3, name: 'JsonServe' },
  { id: 4, name: 'Visual Studio Code' },
  { id: 5, name: 'TypeScript' },
  { id: 6, name: 'React' },
  { id: 7, name: 'Gatsby' },
];
