import * as React from 'react';
import '../static/style/404.scss';

const NotFoundPage = () => (
  <div id="main">
    <div className="fof">
      <h1>Error 404</h1>
    </div>
  </div>
);

export default NotFoundPage;
