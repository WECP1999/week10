import { Chip, Container, Grid } from '@material-ui/core';
import { Code } from '@material-ui/icons';
import { graphql, useStaticQuery, Link } from 'gatsby';
import Img from 'gatsby-image';
import React, { useCallback, useMemo, useState } from 'react';

import FullWithLayout from '../components/layouts/full-width/';
import { Tags } from '../utils/tags';
import '../static/style/projects-style.scss';

type ProjectsType = {
  allDataJson: {
    edges: [
      {
        node: {
          slug: string;
          layout: 'full-width' | 'half-width';
          repository: string;
          siteLink: string;
          tags: number[];
          title: string;
          description: string;
          developmenStack: number[];
          id: number;
          images: any;
        };
      }
    ];
  };
};

const Projects = () => {
  const [filters, setFilters] = useState<typeof Tags>([]);
  const data = useStaticQuery<ProjectsType>(graphql`
    query {
      allDataJson {
        edges {
          node {
            description
            developmenStack
            id
            images {
              url {
                publicURL
                childImageSharp {
                  fluid(maxWidth: 1920, quality: 100) {
                    src
                    srcSet
                    aspectRatio
                    sizes
                    base64
                  }
                }
              }
            }
            repository
            layout
            siteLink
            slug
            tags
            title
          }
        }
      }
    }
  `);

  const { allDataJson } = data;

  const projectsFilteredBy = useMemo(() => {
    const projects = [];

    if (filters.length > 0) {
      allDataJson.edges.forEach(edge => {
        if (
          edge.node.tags.find(tag => filters.find(filter => filter.id === tag))
        ) {
          projects.push(edge);
        }
      });
    } else {
      projects.push(...allDataJson.edges);
    }
    console.log(projects);

    return projects;
  }, [allDataJson, filters]);

  const handleChip = useCallback(
    (tag: { id: number; name: string; description: string }) => {
      if (!filters.find(filter => filter.id === tag.id)) {
        setFilters(filters => [...filters.concat(tag)]);
      } else {
        setFilters(filters => [
          ...filters.filter(filter => filter.id !== tag.id),
        ]);
      }
    },
    [filters]
  );

  return (
    <FullWithLayout>
      <div className="section is-medium">
        <div className="columns">
          <div className="column">
            <h1 className="title is-spaced is-size-1-desktop is-size-2-tablet is-size-3-mobile">
              Filter tags
            </h1>
            <Container maxWidth="lg">
              {Tags.map(tag => (
                <Chip
                  color="secondary"
                  style={
                    tag.id !== Tags[0].id
                      ? {
                          margin: '10px 0 0 10px',
                          backgroundColor: !filters.find(
                            filter => filter.id === tag.id
                          )
                            ? '#141c3a'
                            : '#7510f7',
                        }
                      : {
                          marginTop: '10px',
                          backgroundColor: !filters.find(
                            filter => filter.id === tag.id
                          )
                            ? '#141c3a'
                            : '#7510f7',
                        }
                  }
                  icon={<Code />}
                  key={tag.id}
                  onDelete={undefined}
                  onClick={() => handleChip(tag)}
                  label={tag.name}
                />
              ))}
            </Container>
          </div>
        </div>
        <div className="columns" style={{ margin: '2.5rem 0' }}>
          <div className="column">
            <h1 className="title is-spaced is-size-1-desktop is-size-2-tablet is-size-3-mobile">
              Projects
            </h1>
            <Container maxWidth="lg">
              {projectsFilteredBy.map(project => (
                <Grid
                  container
                  key={project.node.id}
                  justifyContent="center"
                  className="section"
                  spacing={6}
                >
                  <Grid item xs={12} md={6} lg={6} className="image-container">
                    <Link
                      to={`/projects/${project.node.slug}`}
                    >
                      <figure className="layer-image">
                        <Img
                          fluid={
                            project.node.images[0].url.childImageSharp.fluid
                          }
                          className="layer-image__image"
                          alt="image"
                        />
                      </figure>
                    </Link>
                  </Grid>
                  <Grid item xs={12} md={6} lg={6}>
                    <h1 className="title is-spaced is-size-1-desktop is-size-2-tablet is-size-3-mobile">
                      {project.node.title}
                    </h1>
                    <p>{project.node.description}</p>
                    <div className="click-here-button">
                      <Link
                        to={`/projects/${project.node.slug}`}
                        className="button is-primary is-outlined is-rounded"
                      >
                        Click here
                      </Link>
                    </div>
                  </Grid>
                </Grid>
              ))}
            </Container>
          </div>
        </div>
      </div>
    </FullWithLayout>
  );
};

export default Projects;
