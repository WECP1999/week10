/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const { data } = await graphql(`
    query {
      allDataJson {
        nodes {
          slug
        }
      }
    }
  `);

  data.allDataJson.nodes.forEach(node => {
    createPage({
      component: path.resolve('./src/components/templates/projects/index.tsx'),
      path: `/projects/${node.slug}`,
      context: {
        slug: node.slug,
      },
    });
  });
};
